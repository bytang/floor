function ViewModel () {
  var self = this;
  this.views = [ 'Home', 'Projects', 'Servers', 'Contact' ];
  this.currentView = ko.observable();

  this.goToView = function ( view ) {
    location.hash = view;
  };

  Sammy( function() {
    this.get( '#:view', function () {
      self.currentView( this.params.view );
    } );
  } ).run('#Home');
}

ko.applyBindings( new ViewModel() );

