<?php
$nl = '<br>' . PHP_EOL;
$root = $_SERVER['DOCUMENT_ROOT'];

function printDirRecursive($startDir = '', $maxDepth = -1, $depth = 0) {
    GLOBAL $nl, $root;  
    if ($maxDepth == -1 || $depth <= $maxDepth) {
        if ($startDir == '') $startDir = substr(getcwd(), strlen($root));
        if (substr($startDir, -1) != '/') $startDir .= '/';
        $contents = scandir($root . $startDir);
        if ($contents == FALSE) {
            print $startDir . ' is not a directory.';
        } else {            
            $current = '';
            for($i = 0, $length = count($contents); $i < $length; ++$i) {
                $current = $contents[$i];
                if ($current == '.' || $current == '..') { // ignore to prevent infinite loop
                } else {
                    print str_repeat('--', $depth); // visual spacing for tree structure
                    if (is_dir($root . $startDir . $current)) { // a directory
                        print 'directory: ' . $current . $nl;
                        printDirRecursive($startDir . $current . '/', $maxDepth, $depth + 1);
                    } else { // a file              
                        if (substr($current, 0, 1) == '.') {
                            print 'hidden ';
                        }
                        print 'file: ' . $current . $nl;
                    }
                }
            }
        }
    }
}

function findFile($name, $startDir = '', $maxDepth = -1, $depth = 0) {
    GLOBAL $nl, $root;
    $results = [];
    if ($maxDepth == -1 || $depth <= $maxDepth) {
        if ($startDir == '') $startDir = substr(getcwd(), strlen($root));
        if (substr($startDir, -1) != '/') $startDir .= '/';
        $contents = scandir($root . $startDir);
        if ($contents) {
            for($i = 0, $length = count($contents); $i < $length; ++$i) {
                $current = $contents[$i];
                if ($current == '.' || $current == '..') { // ignore to prevent infinite loop
                } else {
                    if (is_dir($root . $startDir . $current)) { // a directory
                        $results = array_merge($results, findFile($name, $startDir . $current . '/', $maxDepth, $depth + 1));
                    } else { // a file              
                        if ($current == $name) {
                            // print 'found ' . $name . ' at ' . $startDir . $nl;
                            $results[] = $startDir . $current;
                        }
                    }
                }
            }
        }
    }
    return $results;
}

printDirRecursive();
?>