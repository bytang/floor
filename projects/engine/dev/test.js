/**
 * Bo Yang Tang
 * boyangtang.ca
 */
(function(){

  var ns = floor,
    mouse = new ns.Mouse("canvas"),
    graphics = new ns.Canvas("canvas"),
    backgroundLayer = graphics.newLayer(),
    //textLayer = graphics.newLayer(),
    statsLayer = graphics.newLayer(),
    //text,
    ms,
    string,
    clock = new ns.Clock(),
    //oldTime = 0,
    time = statsLayer.add(new ns.Text(200, 12)),
    spawnChoice = 'line',
    spawns = 0,
    width = graphics.width / 4,
    height = graphics.height / 4,
    mouselook = false,
    downPos = [0, 0],
    zoomTarget = 1,
    oldZoom = zoomTarget,
    zoomPos = [0, 0],
    headPos = [0, 0],
    lastSpawn = [0, 0],
    dir = ns.random.floatNum(0, 2 * Math.PI),
    change = 0,
    speed = 100,
    timeRatio = 1,
    colourA = ns.random.colour(),
    colourB = ns.random.colour();

  //graphics.fps = 20;
  //textLayer.add(new ns.Text()).id = 'text';
  //text = graphics.getElementById('text');
  //text.string = 'HELLO WORLD\nxD\nayy lmao';
  //text.size = '2';
  //text.colour = 'yellow';
  //text.align = 'center';
  //text.position = 'absolute';
  //text.font = 'Comic Sans MS';
  time.colour = 'red';
  statsLayer.overlay = true;

  function update() {
    ms = Math.floor(clock.milliseconds/100);
    string = (ms/10).toString() + (ms % 10 ? '' : '.0');
    time.string = string;
    //text.string = 'x: ' + mouse.properties.x.toString() + '\ny: ' + mouse.properties.y.toString();
    moveCamera();
    zoomCamera();
  }

  function moveHead() {
    var x, y;
    x = speed * Math.cos(dir) * timeRatio;
    y = speed * Math.sin(dir) * timeRatio;
    headPos[0] += x;
    headPos[1] += y;
    if (ns.random.intNum(0, 4) == 4) { // chance of going straight
      change = 0;
    } else {
      if (ns.random.intNum(0, 10) > 8) {
        change = ns.random.floatNum(-0.1, 0.1);
      }      
    }
    dir += change;
  }

  function grow() {
    var dist = Math.sqrt(Math.pow((headPos[0] - lastSpawn[0]), 2) + Math.pow((headPos[1] - lastSpawn[1]), 2));
    if (dist > 20) {
      spawnObject(lastSpawn);
      lastSpawn[0] = headPos[0];
      lastSpawn[1] = headPos[1];
    }
  }

  function moveCamera() {
    var x = 0,
      y = 0;
    if (mouselook) {
      x = downPos[0] - mouse.x;
      y = downPos[1] - mouse.y;
      graphics.move(x / zoomTarget, y / zoomTarget);
      downPos = [mouse.x, mouse.y];
      //console.log(x, y);
    }    
  }

  function zoomCamera() {
    var x, y, ratio;
    graphics.scale = zoomTarget;

    if (oldZoom != zoomTarget) {
      x = (graphics.render.offset[0] - zoomPos[0]) / zoomTarget;
      y = (graphics.render.offset[1] - zoomPos[1]) / zoomTarget;
      ratio = 1 - (zoomTarget / oldZoom);
      graphics.move(x * ratio, y * ratio);
      oldZoom = zoomTarget;
    }
  }

  graphics.autofill = true;  
  graphics.showStats = true;

  mouse.onWheelScroll('up', function() {
    zoomTarget *= 1.08;
    zoomPos = [mouse.x, mouse.y];
  });

  mouse.onWheelScroll('down', function() {
    zoomTarget /= 1.08;
    zoomPos = [mouse.x, mouse.y];
  });

  mouse.onDown(0, function() {
    mouselook = true;
    downPos = [mouse.x, mouse.y];
    //console.log(downPos);
  });

  mouse.onUp(0, function() {
    mouselook = false;
  });

  mouse.onDown(1, function() {
    graphics.stopRender();
  });

  function spawnObject(pos) {
    var r = ns.random.intNum(20,300),
      a,
      b,
      shape;
    if (spawnChoice == 'circle') {
      shape = new ns.Circle(ns.random.floatNum(pos[0] - 100, headPos[0] + 100), ns.random.floatNum(pos[1] - 300, headPos[1] + 300), r, '#' + ns.random.hexColour());
      //spawnChoice = 'line';
    } else if (spawnChoice == 'line') {
      shape = new ns.Line([pos[0],pos[1]]);
      shape.point = [headPos[0],headPos[1]];
      shape.colour = ns.random.hexColour( colourA, colourB );
      shape.width = 600;
      //spawnChoice = 'arc';
    } else if (spawnChoice == 'arc') {
      a = ns.random.floatNum(0,Math.PI * 2);
      b = ns.random.floatNum(a,Math.PI * 2);
      shape = new ns.Arc(ns.random.floatNum(graphics.center[0], width + graphics.center[0]), ns.random.floatNum(graphics.center[1], height + graphics.center[1]), r, a, b, false, '#' + ns.random.hexColour());
      shape.cc = ns.random.element([true,false]);
      //spawnChoice = 'circle';
    }
    backgroundLayer.add(shape);
    spawns ++;
  }

  /*function test(a, b) {
    var i, start, stop, dir, x, y, z, pos, shape, dX, dY, dZ, range;
    console.log('a: ', a);
    console.log('b: ', b);
    // determine largest range (x, y, or z)
    dX = Math.abs(a[0] - b[0]);
    dY = Math.abs(a[1] - b[1]);
    dZ = Math.abs(a[2] - b[2]);
    range = Math.max(dX, dY, dZ);
    if (range == dX) {
      start = a[0];
      stop = b[0];
      console.log('range x');
    } else if (range == dY) {
      start = a[1];
      stop = b[1];
      console.log('range y');
    } else {
      start = a[2];
      stop = b[2];
      console.log('range z');
    }
    dir = start < stop ? 1 : -1;
    pos = -range/2 * 15;
    for (i = start; i != stop + dir; i += dir) {
      if (range == dX) {
        x = i;
        y = (i - a[0]) * (b[1] - a[1]) / (b[0] - a[0]) + a[1];
        z = (i - a[0]) * (b[2] - a[2]) / (b[0] - a[0]) + a[2];
      } else if (range == dY) {
        x = (i - a[1]) * (b[0] - a[0]) / (b[1] - a[1]) + a[0];
        y = i;
        z = (i - a[1]) * (b[2] - a[2]) / (b[1] - a[1]) + a[2];
      } else {
        x = (i - a[2]) * (b[0] - a[0]) / (b[2] - a[2]) + a[0];
        y = (i - a[2]) * (b[1] - a[1]) / (b[2] - a[2]) + a[1];
        z = i;
      }
      shape = new ns.Line([pos, 0], 0, 10);
      shape.colour = floor.rgbToHex(Math.round(x), Math.round(y), Math.round(z));
      shape.width = 200;
      backgroundLayer.add(shape);
      pos += 15;
      //console.log(x, y, z);
    }
  }*/

  //test(ns.random.colour(), ns.random.colour());

  //spawnObject([0,0]);

  setTimeout(function(){
    clock.start();
    graphics.startRender(update);
    funtimes();
  }, 500);

  function funtimes() {
    setInterval(function() {
      moveHead();
      grow();   
    }, 50);
  }  
})();