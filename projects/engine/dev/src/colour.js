/**
 * @author Bo Yang Tang [bo@boyangtang.ca]
 * @module colour
 */

this.floor = this.floor || {};

(function() {
  /**
   * A namespace for web colour utility functions.
   * @namespace
   */
  floor.colour = floor.colour || {};

  /**
   * Converts hex colour string into RGB array [R, G, B] with integer values.
   *
   * @param {string} [hexString] - hex colour string
   * @returns {?Array}
   */
  floor.colour.hexToRGB = function hexToRGB( hexString ) {
    var i, digits, rgb = [];
    if ( hexString ) {
      if ( hexString.charAt( 0 ) == '#' ) hexString = hexString.substring( 1 );
      if ( hexString.length % 3 ) {
        digits = hexString.length / 3;
        for ( i = 0; i < 3; i++ ) {
          rgb[ i ] = hexString.substr( i * digits, digits );
        }
      }
      if ( rgb.length ) {
        rgb.forEach( function( value, index, array ) {
          array[ index ] = parseInt( value, 16 );
        });
        return rgb;
      }
    }
    return null;
  };

  /**
   * Converts RGB array [R, G, B] with integer values into hex colour string.
   *
   * @param {Array} [rgb] - RGB array
   * @returns {string}
   */
  floor.colour.rgbToHex = function rgbToHex( rgb ) {
    var hexString = '', string = '';
    //console.log(rgb);
    rgb.forEach( function( value ) {
      string = value.toString( 16 );
      if ( string.length == 1 ) string = '0' + string;
      hexString += string;
    });
    return '#' + hexString;
  };

  floor.colour.gradient = function gradient( colourA, colourB ) {
    var red, green, blue, grad = [], dR, dG, dB, range, start, stop, temp, i;

    colourA.forEach( function( value, index, array ) {
      array[ index ] = Math.min( Math.max( 0, value ), 255 );
    });

    colourB.forEach( function( value, index, array ) {
      array[ index ] = Math.min( Math.max( 0, value ), 255 );
    });

    dR = Math.abs( colourA[ 0 ] - colourB[ 0 ] );
    dG = Math.abs( colourA[ 1 ] - colourB[ 1 ] );
    dB = Math.abs( colourA[ 2 ] - colourB[ 2 ] );
    range = Math.max( dR, dG, dB );

    if ( range == dR ) {
      start = colourA[ 0 ];
      stop = colourB[ 0 ];
    } else if ( range == dG ) {
      start = colourA[ 1 ];
      stop = colourB[ 1 ];
    } else {
      start = colourA[ 2 ];
      stop = colourB[ 2 ];
    }

    if ( start > stop ) {
      temp = start;
      start = stop;
      stop = temp;
    }

    for ( i = start; i < stop; i++ ) {
      if ( range == dR ) {
        red = i;
        green = ( i - colourA[ 0 ] ) * ( colourB[ 1 ] - colourA[ 1 ] ) / ( colourB[ 0 ] - colourA[ 0 ] ) + colourA[ 1 ];
        blue = ( i - colourA[ 0 ] ) * ( colourB[ 2 ] - colourA[ 2 ] ) / ( colourB[ 0 ] - colourA[ 0 ] ) + colourA[ 2 ];
      } else if ( range == dG ) {
        red = ( i - colourA[ 1 ] ) * ( colourB[ 0 ] - colourA[ 0 ] ) / ( colourB[ 1 ] - colourA[ 1 ] ) + colourA[ 0 ];
        green = i;
        blue = ( i - colourA[ 1 ] ) * ( colourB[ 2 ] - colourA[ 2 ] ) / ( colourB[ 1 ] - colourA[ 1 ] ) + colourA[ 2 ];
      } else {
        red = ( i - colourA[ 2 ] ) * ( colourB[ 0 ] - colourA[ 0 ] ) / ( colourB[ 2 ] - colourA[ 2 ] ) + colourA[ 0 ];
        green = ( i - colourA[ 2 ] ) * ( colourB[ 1 ] - colourA[ 1 ] ) / ( colourB[ 2 ] - colourA[ 2 ] ) + colourA[ 1 ];
        blue = i;
      }
      grad.push( [ Math.round( red ), Math.round( green ), Math.round( blue ) ] );
    }

    return grad;
  };
})();