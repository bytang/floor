/**
 * Bo's Floor 2015
 */

var cells = {},
    elements = ['completed','completedStats','2PW','2PL','2PStats','3PW','3PL','3PStats','4PW','4PL','4PStats','forfeit','forfeitStats'],
    values = {},
    initFlag = false;

function init() {
    var i = 0,
        end = elements.length;
    for (; i < end; i++) {
        cells[elements[i]] = document.getElementById(elements[i]);
        cells[elements[i]].innerHTML = '0';
        values[elements[i]] = 0;
    }
    initFlag = true;
    console.log('init');
    load();
}

function load() {
    var i = 0,
        end = elements.length,
        storageValue;
    storageValue = localStorage.getItem('ascension.stats.calculator.saved');
    if (storageValue) {
        for (; i < end; i++) {
            storageValue = localStorage.getItem('ascension.stats.calculator.' + elements[i]);
            cells[elements[i]].innerHTML = storageValue;
            values[elements[i]] = parseFloat(storageValue);
        }
    }
    console.log('load');
}

function set(elem, val) {
    console.log('set ' + elem + ' ' + val);
    if (val == '') val = 0;
    val = parseFloat(val);
    if (!isNaN(val)) {
        values[elem] = val;
    }
    cells[elem].innerHTML = values[elem].toString();
    update(elem);
}

function update(elem) {
    console.log('update ' + elem);
    switch(elem) {
        case '2PW':
            add('completed');
        case '2PL':
            set('2PStats', Math.round((values['2PW'] / (values['2PW'] + values['2PL']) * 100) * 10000) / 10000);
            break;
        case '3PW':
            add('completed');
        case '3PL':
            set('3PStats', Math.round((values['3PW'] / (values['3PW'] + values['3PL']) * 100) * 10000) / 10000);
            break;
        case '4PW':
            add('completed');
        case '4PL':
            set('4PStats', Math.round((values['4PW'] / (values['4PW'] + values['4PL']) * 100) * 10000) / 10000);
            break;
        case 'completed':
        case 'forfeit':
            set('completedStats', Math.round((values['completed'] / (values['completed'] + values['forfeit']) * 100) * 10000) / 10000);
            set('forfeitStats', Math.round((100 - values['completedStats']) * 10000) / 10000);
            break;
    }
}

function add(elem) {
    values[elem] += 1;
    cells[elem].innerHTML = values[elem].toString();
    console.log('incr ' + elem)
    update(elem);
}

window.onunload = function() {
    var i = 0,
        end = elements.length;
    if (initFlag) {
        for (; i < end; i++) {
            localStorage.setItem('ascension.stats.calculator.' + elements[i], values[elements[i]]);
        }
        localStorage.setItem('ascension.stats.calculator.saved', 'true');
    }
};