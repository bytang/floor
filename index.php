<?php
$nl = '<br>' . PHP_EOL;
$root = $_SERVER['DOCUMENT_ROOT'];
$projectsPath = '/projects';

function printDirRecursive($startDir = '', $maxDepth = -1, $depth = 0) {
    GLOBAL $nl, $root;
    if ($maxDepth == -1 || $depth <= $maxDepth) {
        if ($startDir == '') $startDir = substr(getcwd(), strlen($root));
        if (substr($startDir, -1) != '/') $startDir .= '/';
        $contents = scandir($root . $startDir);
        if ($contents == FALSE) {
            print $startDir . ' is not a directory.';
        } else {
            $current = '';
            for($i = 0, $length = count($contents); $i < $length; ++$i) {
                $current = $contents[$i];
                if ($current == '.' || $current == '..') { // ignore to prevent infinite loop
                } else {
                    print str_repeat('--', $depth); // visual spacing for tree structure
                    if (is_dir($root . $startDir . $current)) { // a directory
                        print 'directory: ' . $current . $nl;
                        printDirRecursive($startDir . $current . '/', $maxDepth, $depth + 1);
                    } else { // a file
                        if (substr($current, 0, 1) == '.') {
                            print 'hidden ';
                        }
                        print 'file: ' . $current . $nl;
                    }
                }
            }
        }
    }
}

function findFile($name, $startDir = '', $maxDepth = -1, $depth = 0) {
    GLOBAL $root;
    $results = [];
    if ($maxDepth == -1 || $depth <= $maxDepth) {
        if ($startDir == '') $startDir = str_replace('\\', '/', substr(getcwd(), strlen($root)));
        if (substr($startDir, -1) != '/') $startDir .= '/';
        if (substr($startDir, 0, 1) != '/') $startDir = '/' . $startDir;
        $contents = scandir($root . $startDir);
        if ($contents) {
            for($i = 0, $length = count($contents); $i < $length; ++$i) {
                $current = $contents[$i];
                if ($current == '.' || $current == '..') { // ignore to prevent infinite loop
                } else {
                    if (is_dir($root . $startDir . $current)) { // a directory
                        $results = array_merge($results, findFile($name, $startDir . $current . '/', $maxDepth, $depth + 1));
                    } else { // a file
                        if ($current == $name) {
                            // print 'found ' . $name . ' at ' . $startDir . $nl;
                            $results[] = $startDir;
                        }
                    }
                }
            }
        }
    }
    return $results;
}

function process($path) {
    GLOBAL $root, $formatWhitespace, $formatTab, $proj, $projectsPath;
    $json = file_get_contents($root . $path . '.metainfo');
    $info = json_decode($json);
    $output = '';
    $picture = '';
    $description = '';
    $link = $info->{'link'};
    if (substr($link, 0, 1) != '/') $link = $projectsPath . '/' . $link;

    $output .= $formatWhitespace . '<tr>' . PHP_EOL;
    $picture .= $formatWhitespace . $formatTab . '<td class="spaced">' . PHP_EOL .
                $formatWhitespace . $formatTab . $formatTab . '<a href="' . $link . '" target="_blank">'. PHP_EOL;

    if ($info->{'display'} == 'placeholder') {
        $picture .= $formatWhitespace . $formatTab . $formatTab . $formatTab . '<img src="' . '/files/img/placeholder.png' . '" width="100%">' . PHP_EOL;
    } else {
        $picture .= $formatWhitespace . $formatTab . $formatTab . $formatTab . '<img src="' . $path . 'img/' . $info->{'display'} . '" width="100%">' . PHP_EOL;
    }

    $picture .= $formatWhitespace . $formatTab . $formatTab . '</a>' . PHP_EOL .
                $formatWhitespace . $formatTab . '</td>' . PHP_EOL;

    $description .= $formatWhitespace . $formatTab . '<td class="spaced">' . PHP_EOL .
                    $formatWhitespace . $formatTab . $formatTab . $info->{'description'} . PHP_EOL .
                    $formatWhitespace . $formatTab . '</td>' . PHP_EOL;

    if ($proj % 2 == 0) {
        $output .= $picture . $description;
    } else {
        $output .= $description . $picture;
    }
    $proj ++;
    $output .= $formatWhitespace . '</tr>' . PHP_EOL;
    print $output;
}

$projects = findFile('.metainfo', $projectsPath, 1);
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Bo's Floor</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="files/css/style.css">
        <link rel="shortcut icon" type="image/x-icon" href="files/img/favicon.ico">
        <script type="text/javascript" charset="utf-8" src="bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="bower_components/sammy/lib/min/sammy-latest.min.js"></script>
        <script type="text/javascript" charset="utf-8" src="bower_components/knockout/dist/knockout.js"></script>
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1 title="Property of Bo Yang Tang">Bo's Floor</h1>
            </div>
            <div id="menu">
                <ul data-bind="foreach: views">
                    <li data-bind="css: { selected: $data == $root.currentView() }">
                        <a href="" data-bind="text: $data, click: $root.goToView"></a>
                    </li>
                </ul>
            </div>
            <div id="content">
                <div data-bind="visible: currentView() == 'Home'" class="center">
                    <p>Welcome to Bo's Floor, the Internet residence of Bo Yang Tang.</p>
                </div>
                <div data-bind="visible: currentView() == 'Projects'">
                    <table class="center">

<?php
    $formatWhitespace = '                        ';
    $formatTab = '    ';
    $proj = 0;
    foreach($projects as $project) {
        process($project);
    }
?>
                    </table>
                </div>
                <div data-bind="visible: currentView() == 'Servers'">
                    <p>My running servers as of 2016-01-03.</p>
                    <table class="center">
                        <tr>
                            <td>None</td>
                        </tr>
                    </table>
                </div>
                <div data-bind="visible: currentView() == 'Contact'">
                    <p>You can reach me in person or through the tubes. Maybe you'll see me in Duelyst or League.</p>

                    <div id="buttons">
                        <script src="https://apis.google.com/js/platform.js"></script>
                        <div class="g-ytsubscribe" data-channelid="UCR1kshf4PxgXjddYEpu6OJw" data-layout="full" data-theme="dark" data-count="hidden"></div>

                        <a href="https://twitter.com/boyangbotang" target="_blank"><img src="/files/img/twitter_button.png" /></a>

                        <a href="http://www.twitch.tv/boyangtang" target="_blank"><img src="/files/img/twitch_button.png" /></a>

                        <a href="http://steamcommunity.com/id/pariwak/" target="_blank"><img src="http://steamsignature.com/status/english/76561198013174759.png" /></a><a href='steam://friends/add/76561198013174759'><img src='http://steamsignature.com/AddFriend.png'></a>
                    </div>

                    <p>Alternatively, send something to my mailbox at boyangtang dot ca.</p>
                </div>
            </div>
            <div id="footer">
                Bo Yang Tang 2015
            </div>
        </div>
        <script type="text/javascript" charset="utf-8" src="files/js/app.js"></script>
    </body>
</html>